package com.personal.projects.climbing.weatherservice.scheduling;

import com.personal.projects.climbing.weatherservice.data.WeatherUpdateMessage;
import com.personal.projects.climbing.weatherservice.exception.CurrentWeatherException;
import com.personal.projects.climbing.weatherservice.exception.LocationRetrievalException;
import com.personal.projects.climbing.weatherservice.processor.WeatherProcessor;
import com.personal.projects.climbing.weatherservice.rest.LocationServiceEndpoints;
import com.personal.projects.climbing.weatherservice.util.TestUtils;
import com.personal.projects.climbing.weatherservice.writer.WeatherUpdateWriter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

/**
 * Unit tests for the {@link WeatherUpdateTask} class.
 *
 * @author Chris Smith
 */
public class WeatherUpdateTaskTest {
    private int userId = 10;
    private WeatherUpdateTask weatherUpdateTask;

    @Captor
    private ArgumentCaptor<WeatherUpdateMessage> weatherUpdateCaptor;

    @Mock
    private LocationServiceEndpoints locationServiceEndpoints;
    @Mock
    private WeatherProcessor processor;
    @Mock
    private WeatherUpdateWriter writer;

    /**
     * Initialises the mocks, sets up mocked responses and creates an instance of the class under test
     */
    @BeforeEach
    public void setup() throws LocationRetrievalException {
        MockitoAnnotations.initMocks(this);
        this.weatherUpdateTask = new WeatherUpdateTask(this.userId, this.locationServiceEndpoints, this.processor, this.writer);
        when(this.locationServiceEndpoints.getOutdoorClimbingLocations(this.userId)).thenReturn(
                Arrays.asList(
                        TestUtils.createDummyClimbingLocation(),
                        TestUtils.createDummyClimbingLocation(),
                        TestUtils.createDummyClimbingLocation()));

    }

    /**
     * Tests that {@link WeatherUpdateTask#run()} calls all the expected external methods the expected number of times
     *
     * @throws CurrentWeatherException
     * @throws LocationRetrievalException
     */
    @Test
    public void run_validState_callsMethodsExpectedNoOfTimes() throws CurrentWeatherException, LocationRetrievalException {
        // Arrange
        when(this.processor.processCurrentWeatherRequest(anyString(), anyString()))
                .thenReturn(TestUtils.createDummyCurrentWeather());
        int expectedWeatherUpdates = 3;

        // Act
        this.weatherUpdateTask.run();

        // Assert
        verify(this.locationServiceEndpoints, times(1)).getOutdoorClimbingLocations(this.userId);
        verify(this.processor, times(3)).processCurrentWeatherRequest(anyString(), anyString());
        verify(this.writer, times(1)).sendWeatherUpdate(this.weatherUpdateCaptor.capture());
        WeatherUpdateMessage captorValue = this.weatherUpdateCaptor.getValue();
        assertEquals(expectedWeatherUpdates, captorValue.getUpdates().size());
    }

    /**
     * Tests that {@link WeatherUpdateTask#run()} creates a {@link WeatherUpdateMessage} containing the expected
     * number of {@link com.personal.projects.climbing.weatherservice.data.WeatherUpdate}s, if there is an error when
     * trying to retrieve the current weather for one of the locations
     *
     * @throws CurrentWeatherException
     */
    @Test
    public void run_errorRetrievingCurrentWeather_correctNoOfWeatherUpdates() throws CurrentWeatherException {
        // Arrange
        when(this.processor.processCurrentWeatherRequest(anyString(), anyString()))
                .thenReturn(TestUtils.createDummyCurrentWeather())
                .thenThrow(new CurrentWeatherException())
                .thenReturn(TestUtils.createDummyCurrentWeather());
        int expectedWeatherUpdates = 2;

        // Act
        this.weatherUpdateTask.run();

        // Assert
        verify(this.writer, times(1)).sendWeatherUpdate(this.weatherUpdateCaptor.capture());
        WeatherUpdateMessage captorValue = this.weatherUpdateCaptor.getValue();
        assertEquals(expectedWeatherUpdates, captorValue.getUpdates().size());
    }
}
