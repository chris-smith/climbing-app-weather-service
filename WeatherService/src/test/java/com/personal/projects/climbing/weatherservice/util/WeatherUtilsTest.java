package com.personal.projects.climbing.weatherservice.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Unit tests for the {@link WeatherUtils} class.
 *
 * @author Chris Smith
 */
public class WeatherUtilsTest {
    /**
     * Tests that {@link WeatherUtils#windDirectionDegreesToCompass(double)} returns the correct value when called
     * with a valid argument
     */
    @Test
    public void windDirectionDegreesToCompass_validInput_returnsExpectedValue() {
        // Arrange
        double windDirectionDegrees = 63.5;
        String expectedWindDirectionCompass = "ENE";

        // Act
        String actualWindDirectionCompass = WeatherUtils.windDirectionDegreesToCompass(windDirectionDegrees);

        // Assert
        assertEquals(expectedWindDirectionCompass, actualWindDirectionCompass);
    }
}
