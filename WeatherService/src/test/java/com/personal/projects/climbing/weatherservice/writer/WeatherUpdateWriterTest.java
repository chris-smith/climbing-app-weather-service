package com.personal.projects.climbing.weatherservice.writer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.personal.projects.climbing.weatherservice.data.WeatherUpdateMessage;
import com.personal.projects.climbing.weatherservice.util.TestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

/**
 * Unit tests for the {@link WeatherUpdateWriter} class.
 *
 * @author Chris Smith
 */
public class WeatherUpdateWriterTest {
    private String topic = "weather-update";
    private WeatherUpdateMessage dummyWeatherUpdateMessage;

    @Mock
    private KafkaTemplate<String, String> kafkaTemplate;

    @InjectMocks
    private WeatherUpdateWriter writer;

    /**
     * Initialises the mocks and sets any necessary fields on the class under test
     */
    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(this.writer, "topic", this.topic);
        this.dummyWeatherUpdateMessage = TestUtils.createDummyWeatherUpdateMessage();
    }

    /**
     * Tests that {@link WeatherUpdateWriter#sendWeatherUpdate(WeatherUpdateMessage)} calls
     * {@link KafkaTemplate#send(String, Object)} and returns true when a valid {@link WeatherUpdateMessage} is supplied
     */
    @Test
    public void sendWeatherUpdate_validWeatherUpdateMessage_callsKafkaTemplateAndReturnsTrue() {
        // Act
        boolean success = this.writer.sendWeatherUpdate(this.dummyWeatherUpdateMessage);

        // Assert
        verify(this.kafkaTemplate, times(1)).send(eq(this.topic), anyString());
        assertTrue(success);
    }

    /**
     * Tests that {@link WeatherUpdateWriter#sendWeatherUpdate(WeatherUpdateMessage)} returns false and does not call
     * {@link KafkaTemplate#send(String, Object)}, when a {@link JsonProcessingException} is thrown during conversion of
     * the supplied {@link WeatherUpdateMessage} to JSON
     *
     * @throws JsonProcessingException
     */
    @Test
    public void sendWeatherUpdate_errorConvertingToJson_returnsFalseAndNoInteractionWithKafkaTemplate()
            throws JsonProcessingException {
        // Arrange
        WeatherUpdateMessage mockWeatherUpdateMessage = mock(WeatherUpdateMessage.class);
        when(mockWeatherUpdateMessage.toJson()).thenThrow(new JsonProcessingException("Could not convert to JSON") {
        });

        // Act
        boolean success = this.writer.sendWeatherUpdate(mockWeatherUpdateMessage);

        // Assert
        verify(this.kafkaTemplate, never()).send(anyString(), anyString());
        assertFalse(success);
    }
}
