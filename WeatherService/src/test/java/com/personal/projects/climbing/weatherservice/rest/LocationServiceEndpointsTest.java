package com.personal.projects.climbing.weatherservice.rest;

import com.personal.projects.climbing.weatherservice.exception.LocationRetrievalException;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.reactive.function.client.WebClient;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Unit tests for the {@link LocationServiceEndpoints} class.
 *
 * @author Chris Smith
 */
public class LocationServiceEndpointsTest {
    private MockWebServer mockWebServer;

    @InjectMocks
    private LocationServiceEndpoints endpoints;

    /**
     * Starts a {@link MockWebServer} to handle the REST calls made by {@link LocationServiceEndpoints}
     *
     * @throws IOException
     */
    @BeforeEach
    public void setUp() throws IOException {
        this.mockWebServer = new MockWebServer();
        this.mockWebServer.start();

        MockitoAnnotations.initMocks(this);
        WebClient webClient = WebClient
                .builder()
                .clientConnector(new ReactorClientHttpConnector())
                .baseUrl(this.mockWebServer.url("/").toString())
                .build();

        ReflectionTestUtils.setField(this.endpoints, "webClient", webClient);
        ReflectionTestUtils.setField(
                this.endpoints, "outdoorClimbingLocationsUrlTemplate", this.mockWebServer.url("/").toString());
    }

    /**
     * Shuts down the {@link MockWebServer} after each test
     *
     * @throws IOException
     */
    @AfterEach
    public void tearDown() throws IOException {
        this.mockWebServer.shutdown();
    }

    /**
     * Tests that {@link LocationServiceEndpoints#getOutdoorClimbingLocations(int)} throws a
     * {@link LocationRetrievalException} when an error response is returned from Location Service
     */
    @Test
    public void getOutdoorClimbingLocations_errorWithRestResponse_throwsLocationRetrievalException() {
        // Arrange
        mockWebServer.enqueue(
                new MockResponse()
                        .setResponseCode(500)
                        .setBody("Error")
                        .setHeader("Content-Type", "application/json"));

        int userId = 5;

        // Act / Assert
        assertThrows(LocationRetrievalException.class, () -> this.endpoints.getOutdoorClimbingLocations(userId));
    }
}
