package com.personal.projects.climbing.weatherservice.util;

import com.personal.projects.climbing.weatherservice.data.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Utility methods to be used throughout the tests.
 *
 * @author Chris Smith
 */
public final class TestUtils {
    /**
     * Default private constructor to prevent instantiation
     */
    private TestUtils() {

    }

    /**
     * Creates a dummy {@link ClimbingLocation} object to be used in the tests
     *
     * @return the dummy {@link ClimbingLocation} object
     */
    public static ClimbingLocation createDummyClimbingLocation() {
        ClimbingLocation location = new ClimbingLocation();
        location.setUserId(1);
        location.setName("The Cuttings, Portland");
        location.setLocality("Portland");
        location.setCounty("Dorset");
        location.setCountry("United Kingdom");
        location.setPostCode("PORTLAND");
        location.setLatitude(1.1111);
        location.setLongitude(-1.1111);
        return location;
    }

    /**
     * Creates a dummy {@link CurrentWeather} object to be used in the tests
     *
     * @return the dummy {@link CurrentWeather} object
     */
    public static CurrentWeather createDummyCurrentWeather() {
        CurrentWeather currentWeather = new CurrentWeather();

        Coordinates coordinates = new Coordinates();
        coordinates.setLatitude(50.723);
        coordinates.setLongitude(-1.8996);

        WeatherOverview weatherOverview = new WeatherOverview();
        weatherOverview.setCondition("Clouds");
        weatherOverview.setDescription("Scattered clouds");

        WeatherMetrics weatherMetrics = new WeatherMetrics();
        weatherMetrics.setTemperature(6.8);
        weatherMetrics.setFeelsLikeTemp(0.8);
        weatherMetrics.setMinTemp(6.11);
        weatherMetrics.setMaxTemp(7.22);
        weatherMetrics.setHumidity(49.0);

        DaylightInfo daylightInfo = new DaylightInfo();
        daylightInfo.setSunrise(1615012869L);
        daylightInfo.setSunset(1615053410L);

        WindInfo windInfo = new WindInfo();
        windInfo.setWindSpeed(5.66);
        windInfo.setWindDirectionDegrees(80);

        currentWeather.setCoordinates(coordinates);
        currentWeather.setWeatherOverview(Collections.singletonList(weatherOverview));
        currentWeather.setWeatherMetrics(weatherMetrics);
        currentWeather.setVisibility(10000);
        currentWeather.setDaylightInfo(daylightInfo);
        currentWeather.setWindInfo(windInfo);

        return currentWeather;
    }

    /**
     * Creates a dummy {@link HourlyWeatherForecastResponse} object to be used in the tests
     *
     * @return the dummy {@link HourlyWeatherForecastResponse} object
     */
    public static HourlyWeatherForecastResponse createDummyHourlyForecast() {
        HourlyWeatherForecastResponse hourlyWeatherForecastResponse = new HourlyWeatherForecastResponse();
        List<HourlyWeatherForecast> hourlyForecasts = new ArrayList<HourlyWeatherForecast>();

        HourlyWeatherForecast hourlyForecast = new HourlyWeatherForecast();
        hourlyForecast.setTimeOfForecast(1615012869L);
        hourlyForecast.setTemperature(20.0);
        hourlyForecast.setFeelsLikeTemp(18.4);
        hourlyForecast.setHumidity(56.0);
        hourlyForecast.setUvIndex(0.0);
        hourlyForecast.setCloudiness(20.0);
        hourlyForecast.setVisibility(10000.0);
        hourlyForecast.setWindSpeed(1.87);
        hourlyForecast.setWindDirectionCompass("SW");
        hourlyForecast.setChanceOfRain(0.0);


        WeatherOverview weatherOverview = new WeatherOverview();
        weatherOverview.setCondition("Clouds");
        weatherOverview.setDescription("Scattered clouds");
        hourlyForecast.setWeatherOverview(Collections.singletonList(weatherOverview));

        hourlyForecasts.add(hourlyForecast);
        hourlyWeatherForecastResponse.setResults(hourlyForecasts);
        hourlyWeatherForecastResponse.setLongitude(-1.11);
        hourlyWeatherForecastResponse.setLatitude(1.11);
        hourlyWeatherForecastResponse.setTimezone("Europe/London");
        hourlyWeatherForecastResponse.setTimezoneOffset(0);
        return hourlyWeatherForecastResponse;
    }

    /**
     * Creates a dummy {@link DailyWeatherForecastResponse} object to be used in the tests
     *
     * @return the dummy {@link DailyWeatherForecastResponse} object
     */
    public static DailyWeatherForecastResponse createDummyDailyForecast() {
        DailyWeatherForecastResponse dailyWeatherForecastResponse = new DailyWeatherForecastResponse();
        List<DailyWeatherForecast> dailyForecasts = new ArrayList<DailyWeatherForecast>();

        DailyWeatherForecast dailyForecast = new DailyWeatherForecast();
        dailyForecast.setTimeOfForecast(1615012869L);
        dailyForecast.setSunrise(1615012869L);
        dailyForecast.setSunset(1615012869L);
        dailyForecast.setHumidity(56.0);
        dailyForecast.setUvIndex(3.0);
        dailyForecast.setCloudiness(100.0);
        dailyForecast.setWindSpeed(2.01);
        dailyForecast.setWindDirectionCompass("NW");
        dailyForecast.setChanceOfRain(0.57);

        DailyTemp dailyTemp = new DailyTemp();
        dailyTemp.setDayTemp(7.87);
        dailyTemp.setMinTemp(4.34);
        dailyTemp.setMaxTemp(7.96);
        dailyTemp.setNightTemp(6.26);
        dailyTemp.setEveningTemp(6.46);
        dailyTemp.setMorningTemp(5.73);
        dailyForecast.setTemperature(dailyTemp);

        DailyFeelsLikeTemp dailyFeelsLikeTemp = new DailyFeelsLikeTemp();
        dailyFeelsLikeTemp.setDayFeelsLikeTemp(5.16);
        dailyFeelsLikeTemp.setNightFeelsLikeTemp(4.64);
        dailyFeelsLikeTemp.setEveningFeelsLikeTemp(4.46);
        dailyFeelsLikeTemp.setMorningFeelsLikeTemp(2.91);
        dailyForecast.setFeelsLikeTemp(dailyFeelsLikeTemp);

        WeatherOverview weatherOverview = new WeatherOverview();
        weatherOverview.setCondition("Clouds");
        weatherOverview.setDescription("Scattered clouds");
        dailyForecast.setWeatherOverview(Collections.singletonList(weatherOverview));

        dailyForecasts.add(dailyForecast);
        dailyWeatherForecastResponse.setResults(dailyForecasts);
        dailyWeatherForecastResponse.setLongitude(-1.11);
        dailyWeatherForecastResponse.setLatitude(1.11);
        dailyWeatherForecastResponse.setTimezone("Europe/London");
        dailyWeatherForecastResponse.setTimezoneOffset(0);
        return dailyWeatherForecastResponse;
    }

    /**
     * Creates a dummy {@link WeatherUpdateMessage} object to be used in the tests
     *
     * @return the dummy {@link WeatherUpdateMessage} object
     */
    public static WeatherUpdateMessage createDummyWeatherUpdateMessage() {
        WeatherUpdate weatherUpdate = new WeatherUpdate(createDummyClimbingLocation(), createDummyCurrentWeather());
        return new WeatherUpdateMessage(100, Collections.singletonList(weatherUpdate));
    }
}
