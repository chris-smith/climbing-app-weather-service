package com.personal.projects.climbing.weatherservice.rest;

import com.personal.projects.climbing.weatherservice.exception.CurrentWeatherException;
import com.personal.projects.climbing.weatherservice.exception.WeatherForecastException;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.reactive.function.client.WebClient;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Unit tests for the {@link OpenWeatherMapEndpoints} class.
 *
 * @author Chris Smith
 */
public class OpenWeatherMapEndpointsTest {
    private MockWebServer mockWebServer;

    @InjectMocks
    private OpenWeatherMapEndpoints endpoints;

    /**
     * Starts a {@link MockWebServer} to handle the REST calls made by {@link OpenWeatherMapEndpoints}
     *
     * @throws IOException
     */
    @BeforeEach
    public void setUp() throws IOException {
        this.mockWebServer = new MockWebServer();
        this.mockWebServer.start();

        MockitoAnnotations.initMocks(this);
        WebClient webClient = WebClient
                .builder()
                .clientConnector(new ReactorClientHttpConnector())
                .baseUrl(this.mockWebServer.url("/").toString())
                .build();

        ReflectionTestUtils.setField(this.endpoints, "webClient", webClient);
        ReflectionTestUtils.setField(
                this.endpoints, "currentWeatherUrlTemplate", this.mockWebServer.url("/").toString());
        ReflectionTestUtils.setField(
                this.endpoints, "hourlyForecastUrlTemplate", this.mockWebServer.url("/").toString());
        ReflectionTestUtils.setField(
                this.endpoints, "dailyForecastUrlTemplate", this.mockWebServer.url("/").toString());
    }

    /**
     * Shuts down the {@link MockWebServer} after each test
     *
     * @throws IOException
     */
    @AfterEach
    public void tearDown() throws IOException {
        this.mockWebServer.shutdown();
    }

    /**
     * Tests that {@link OpenWeatherMapEndpoints#getCurrentWeather(String, String)} throws a
     * {@link CurrentWeatherException} when an error is returned from OpenWeatherMap's 'Current Weather' API
     */
    @Test
    public void getCurrentWeather_errorWithRestResponse_throwsCurrentWeatherException() {
        // Arrange
        mockWebServer.enqueue(
                new MockResponse()
                        .setResponseCode(500)
                        .setBody("Error")
                        .setHeader("Content-Type", "application/json"));

        String latitude = "1.11";
        String longitude = "-1.11";

        // Act / Assert
        assertThrows(CurrentWeatherException.class, () -> this.endpoints.getCurrentWeather(latitude, longitude));
    }

    /**
     * Tests that {@link OpenWeatherMapEndpoints#getHourlyWeatherForecast(String, String)} throws a
     * {@link WeatherForecastException} when an error is returned from OpenWeatherMap's 'One Call' weather forecast API
     */
    @Test
    public void getHourlyWeatherForecast_errorWithRestResponse_throwsWeatherForecastException() {
        // Arrange
        mockWebServer.enqueue(
                new MockResponse()
                        .setResponseCode(500)
                        .setBody("Error")
                        .setHeader("Content-Type", "application/json"));

        String latitude = "1.11";
        String longitude = "-1.11";

        // Act / Assert
        assertThrows(WeatherForecastException.class, () -> this.endpoints.getHourlyWeatherForecast(latitude, longitude));
    }

    /**
     * Tests that {@link OpenWeatherMapEndpoints#getDailyWeatherForecast(String, String)} throws a
     * {@link WeatherForecastException} when an error is returned from OpenWeatherMap's 'One Call' weather forecast API
     */
    @Test
    public void getDailyWeatherForecast_errorWithRestResponse_throwsWeatherForecastException() {
        // Arrange
        mockWebServer.enqueue(
                new MockResponse()
                        .setResponseCode(500)
                        .setBody("Error")
                        .setHeader("Content-Type", "application/json"));

        String latitude = "1.11";
        String longitude = "-1.11";

        // Act / Assert
        assertThrows(WeatherForecastException.class, () -> this.endpoints.getDailyWeatherForecast(latitude, longitude));
    }
}
