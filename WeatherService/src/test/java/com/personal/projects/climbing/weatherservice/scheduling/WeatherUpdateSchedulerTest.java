package com.personal.projects.climbing.weatherservice.scheduling;

import com.personal.projects.climbing.weatherservice.processor.WeatherProcessor;
import com.personal.projects.climbing.weatherservice.rest.LocationServiceEndpoints;
import com.personal.projects.climbing.weatherservice.writer.WeatherUpdateWriter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Unit tests for the {@link WeatherUpdateScheduler} class.
 *
 * @author Chris Smith
 */
public class WeatherUpdateSchedulerTest {
    @Mock
    private LocationServiceEndpoints locationServiceEndpoints;

    @Mock
    private WeatherProcessor processor;

    @Mock
    private WeatherUpdateWriter writer;

    @InjectMocks
    private WeatherUpdateScheduler scheduler;

    /**
     * Initialises the mocks
     */
    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Tests that {@link WeatherUpdateScheduler#scheduleTask(int, int)} creates a new {@link ScheduledExecutorService}
     * and stores it in its list of schedules
     */
    @Test
    @SuppressWarnings("unchecked")
    public void scheduleTask_validInputArgs_createsNewSchedule() {
        // Arrange
        int taskId = 10;
        int intervalSeconds = 60;

        // Act
        this.scheduler.scheduleTask(taskId, intervalSeconds);

        // Assert
        Map<Integer, ScheduledExecutorService> scheduledTasks =
                (ConcurrentHashMap<Integer, ScheduledExecutorService>) ReflectionTestUtils.getField(
                        this.scheduler,
                        "weatherUpdateTasks");
        assertEquals(1, scheduledTasks.size());
    }

    /**
     * Tests that {@link WeatherUpdateScheduler#stopTask(int)} removes the specified
     * task/{@link ScheduledExecutorService} from its list of schedules
     */
    @Test
    @SuppressWarnings("unchecked")
    public void stopTask_validInputArgs_removesSchedule() {
        // Arrange
        int taskId = 10;
        Map<Integer, ScheduledExecutorService> scheduledTasksBefore =
                new ConcurrentHashMap<Integer, ScheduledExecutorService>() {{
                    put(taskId, Executors.newSingleThreadScheduledExecutor());
                }};
        ReflectionTestUtils.setField(this.scheduler, "weatherUpdateTasks", scheduledTasksBefore);

        // Act
        this.scheduler.stopTask(taskId);

        // Assert
        Map<Integer, ScheduledExecutorService> scheduledTasksAfter =
                (ConcurrentHashMap<Integer, ScheduledExecutorService>) ReflectionTestUtils.getField(
                        this.scheduler,
                        "weatherUpdateTasks");
        assertEquals(0, scheduledTasksAfter.size());
    }
}
