package com.personal.projects.climbing.weatherservice.rest;

import com.personal.projects.climbing.weatherservice.data.CurrentWeather;
import com.personal.projects.climbing.weatherservice.data.DailyWeatherForecastResponse;
import com.personal.projects.climbing.weatherservice.data.HourlyWeatherForecastResponse;
import com.personal.projects.climbing.weatherservice.data.WeatherUpdateRequest;
import com.personal.projects.climbing.weatherservice.enums.ForecastType;
import com.personal.projects.climbing.weatherservice.exception.CurrentWeatherException;
import com.personal.projects.climbing.weatherservice.exception.WeatherForecastException;
import com.personal.projects.climbing.weatherservice.processor.WeatherProcessor;
import com.personal.projects.climbing.weatherservice.scheduling.WeatherUpdateScheduler;
import com.personal.projects.climbing.weatherservice.util.TestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Unit tests for the {@link WeatherController} class.
 *
 * @author Chris Smith
 */
public class WeatherControllerTest {
    private CurrentWeather dummyCurrentWeather;
    private HourlyWeatherForecastResponse dummyHourlyForecastResponse;
    private DailyWeatherForecastResponse dummyDailyForecastResponse;

    @Mock
    private WeatherProcessor processor;

    @Mock
    private WeatherUpdateScheduler weatherUpdateScheduler;

    @InjectMocks
    private WeatherController controller;

    /**
     * Initialises the mocks and creates dummy objects to be used in the tests
     */
    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.dummyCurrentWeather = TestUtils.createDummyCurrentWeather();
        this.dummyHourlyForecastResponse = TestUtils.createDummyHourlyForecast();
        this.dummyDailyForecastResponse = TestUtils.createDummyDailyForecast();
    }

    /**
     * Tests that {@link WeatherController#getCurrentWeather(String, String)} returns the expected response when
     * a valid latitude and longitude are supplied
     *
     * @throws CurrentWeatherException
     */
    @Test
    public void getCurrentWeather_validRequest_returnsExpectedResponse() throws CurrentWeatherException {
        // Arrange
        String latitude = "1.11";
        String longitude = "-1.11";
        when(this.processor.processCurrentWeatherRequest(latitude, longitude)).thenReturn(this.dummyCurrentWeather);

        // Act
        ResponseEntity<?> response = this.controller.getCurrentWeather(latitude, longitude);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(this.dummyCurrentWeather, response.getBody());
    }

    /**
     * Tests that {@link WeatherController#getCurrentWeather(String, String)} calls
     * {@link WeatherProcessor#processCurrentWeatherRequest(String, String)} the expected number of times when
     * a valid latitude and longitude are supplied
     *
     * @throws CurrentWeatherException
     */
    @Test
    public void getCurrentWeather_validRequest_callsProcessorExpectedNoOfTimes() throws CurrentWeatherException {
        // Arrange
        String latitude = "1.11";
        String longitude = "-1.11";

        // Act
        this.controller.getCurrentWeather(latitude, longitude);

        // Assert
        verify(this.processor, times(1)).processCurrentWeatherRequest(latitude, longitude);
    }

    /**
     * Tests that {@link WeatherController#getCurrentWeather(String, String)} returns a
     * {@link HttpStatus#INTERNAL_SERVER_ERROR} response when the required data cannot be retrieved from one of the
     * third party APIs
     *
     * @throws CurrentWeatherException
     */
    @Test
    public void getCurrentWeather_errorResponseFromThirdPartyApi_returns500ServerError() throws CurrentWeatherException {
        // Arrange
        String latitude = "1.11";
        String longitude = "-1.11";
        String expectedErrorMessage = String.format("Error retrieving current weather for latitude %s, longitude %s",
                latitude, longitude);
        when(this.processor.processCurrentWeatherRequest(latitude, longitude)).thenReturn(null);

        // Act
        ResponseEntity<?> response = this.controller.getCurrentWeather(latitude, longitude);

        // Assert
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals(expectedErrorMessage, response.getBody());
    }

    /**
     * Tests that {@link WeatherController#getHourlyWeatherForecast(String, String)} returns the expected response when
     * a valid latitude and longitude are supplied
     *
     * @throws WeatherForecastException
     */
    @Test
    public void getHourlyWeatherForecast_validRequest_returnsExpectedResponse() throws WeatherForecastException {
        // Arrange
        String latitude = "1.11";
        String longitude = "-1.11";
        ForecastType forecastType = ForecastType.HOURLY;
        when(this.processor.processWeatherForecastRequest(latitude, longitude, forecastType))
                .thenReturn(this.dummyHourlyForecastResponse);

        // Act
        ResponseEntity<?> response = this.controller.getHourlyWeatherForecast(latitude, longitude);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(this.dummyHourlyForecastResponse, response.getBody());
    }

    /**
     * Tests that {@link WeatherController#getHourlyWeatherForecast(String, String)} calls
     * {@link WeatherProcessor#processWeatherForecastRequest(String, String, ForecastType)} the expected number of
     * times when a valid latitude and longitude are supplied
     *
     * @throws WeatherForecastException
     */
    @Test
    public void getHourlyWeatherForecast_validRequest_callsProcessorExpectedNoOfTimes()
            throws WeatherForecastException {
        // Arrange
        String latitude = "1.11";
        String longitude = "-1.11";
        ForecastType forecastType = ForecastType.HOURLY;

        // Act
        this.controller.getHourlyWeatherForecast(latitude, longitude);

        // Assert
        verify(this.processor, times(1))
                .processWeatherForecastRequest(latitude, longitude, forecastType);
    }

    /**
     * Tests that {@link WeatherController#getHourlyWeatherForecast(String, String)} returns a
     * {@link HttpStatus#INTERNAL_SERVER_ERROR} response when the required data cannot be retrieved from
     * OpenWeatherMap's weather forecast API
     *
     * @throws WeatherForecastException
     */
    @Test
    public void getHourlyWeatherForecast_errorResponseFromThirdPartyApi_returns500ServerError()
            throws WeatherForecastException {
        // Arrange
        String latitude = "1.11";
        String longitude = "-1.11";
        ForecastType forecastType = ForecastType.HOURLY;
        String expectedErrorMessage = String.format("Error retrieving hourly weather forecast for latitude %s, " +
                "longitude %s", latitude, longitude);
        when(this.processor.processWeatherForecastRequest(latitude, longitude, forecastType))
                .thenThrow(new WeatherForecastException());

        // Act
        ResponseEntity<?> response = this.controller.getHourlyWeatherForecast(latitude, longitude);

        // Assert
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals(expectedErrorMessage, response.getBody());
    }

    /**
     * Tests that {@link WeatherController#getDailyWeatherForecast(String, String)} returns the expected response when
     * a valid latitude and longitude are supplied
     *
     * @throws WeatherForecastException
     */
    @Test
    public void getDailyWeatherForecast_validRequest_returnsExpectedResponse() throws WeatherForecastException {
        // Arrange
        String latitude = "1.11";
        String longitude = "-1.11";
        ForecastType forecastType = ForecastType.DAILY;
        when(this.processor.processWeatherForecastRequest(latitude, longitude, forecastType))
                .thenReturn(this.dummyDailyForecastResponse);

        // Act
        ResponseEntity<?> response = this.controller.getDailyWeatherForecast(latitude, longitude);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(this.dummyDailyForecastResponse, response.getBody());
    }

    /**
     * Tests that {@link WeatherController#getDailyWeatherForecast(String, String)} calls
     * {@link WeatherProcessor#processWeatherForecastRequest(String, String, ForecastType)} the expected number of
     * times when a valid latitude and longitude are supplied
     *
     * @throws WeatherForecastException
     */
    @Test
    public void getDailyWeatherForecast_validRequest_callsProcessorExpectedNoOfTimes()
            throws WeatherForecastException {
        // Arrange
        String latitude = "1.11";
        String longitude = "-1.11";
        ForecastType forecastType = ForecastType.DAILY;

        // Act
        this.controller.getDailyWeatherForecast(latitude, longitude);

        // Assert
        verify(this.processor, times(1))
                .processWeatherForecastRequest(latitude, longitude, forecastType);
    }

    /**
     * Tests that {@link WeatherController#getDailyWeatherForecast(String, String)} returns a
     * {@link HttpStatus#INTERNAL_SERVER_ERROR} response when the required data cannot be retrieved from
     * OpenWeatherMap's weather forecast API
     *
     * @throws WeatherForecastException
     */
    @Test
    public void getDailyWeatherForecast_errorResponseFromThirdPartyApi_returns500ServerError()
            throws WeatherForecastException {
        // Arrange
        String latitude = "1.11";
        String longitude = "-1.11";
        ForecastType forecastType = ForecastType.DAILY;
        String expectedErrorMessage = String.format("Error retrieving daily weather forecast for latitude %s, " +
                "longitude %s", latitude, longitude);
        when(this.processor.processWeatherForecastRequest(latitude, longitude, forecastType))
                .thenThrow(new WeatherForecastException());

        // Act
        ResponseEntity<?> response = this.controller.getDailyWeatherForecast(latitude, longitude);

        // Assert
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals(expectedErrorMessage, response.getBody());
    }

    /**
     * Tests that {@link WeatherController#startWeatherUpdateTask(WeatherUpdateRequest)} calls
     * {@link WeatherUpdateScheduler#scheduleTask(int, int)} the expected number of times when a valid
     * {@link WeatherUpdateRequest} is supplied
     */
    @Test
    public void startWeatherUpdateTask_validRequest_callsSchedulerExpectedNoOfTimes() {
        // Arrange
        int userId = 10;
        int intervalSeconds = 60;
        WeatherUpdateRequest weatherUpdateRequest = new WeatherUpdateRequest();
        weatherUpdateRequest.setUserId(userId);
        weatherUpdateRequest.setIntervalSeconds(intervalSeconds);

        // Act
        ResponseEntity<?> response = this.controller.startWeatherUpdateTask(weatherUpdateRequest);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(this.weatherUpdateScheduler, times(1)).scheduleTask(userId, intervalSeconds);
    }

    /**
     * Tests that {@link WeatherController#stopWeatherUpdateTask(int)} calls
     * {@link WeatherUpdateScheduler#stopTask(int)} the expected number of times
     */
    @Test
    public void stopWeatherUpdateTask_validRequest_callsSchedulerExpectedNoOfTimes() {
        // Arrange
        int id = 10;

        // Act
        ResponseEntity<?> response = this.controller.stopWeatherUpdateTask(id);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(this.weatherUpdateScheduler, times(1)).stopTask(id);
    }
}
