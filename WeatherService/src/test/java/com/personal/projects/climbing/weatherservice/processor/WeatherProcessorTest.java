package com.personal.projects.climbing.weatherservice.processor;

import com.personal.projects.climbing.weatherservice.enums.ForecastType;
import com.personal.projects.climbing.weatherservice.exception.WeatherForecastException;
import com.personal.projects.climbing.weatherservice.rest.OpenWeatherMapEndpoints;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Unit tests for the {@link WeatherProcessor} class.
 *
 * @author Chris Smith
 */
public class WeatherProcessorTest {
    @Mock
    private OpenWeatherMapEndpoints openWeatherMapEndpoints;

    @InjectMocks
    private WeatherProcessor processor;

    /**
     * Initialises the mocks
     */
    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Tests that {@link WeatherProcessor#processWeatherForecastRequest(String, String, ForecastType)} calls
     * {@link OpenWeatherMapEndpoints#getHourlyWeatherForecast(String, String)} the expected number of times when
     * the forecast type parameter equals {@link ForecastType#HOURLY}
     *
     * @throws WeatherForecastException
     */
    @Test
    public void processWeatherForecastRequest_hourlyForecastRequest_callsCorrectEndpointMethod()
            throws WeatherForecastException {
        // Arrange
        String latitude = "1.11";
        String longitude = "-1.11";
        ForecastType forecastType = ForecastType.HOURLY;

        // Act
        this.processor.processWeatherForecastRequest(latitude, longitude, forecastType);

        // Assert
        verify(this.openWeatherMapEndpoints, times(1)).getHourlyWeatherForecast(latitude, longitude);
    }

    /**
     * Tests that {@link WeatherProcessor#processWeatherForecastRequest(String, String, ForecastType)} calls
     * {@link OpenWeatherMapEndpoints#getDailyWeatherForecast(String, String)} the expected number of times when
     * the forecast type parameter equals {@link ForecastType#DAILY}
     *
     * @throws WeatherForecastException
     */
    @Test
    public void processWeatherForecastRequest_dailyForecastRequest_callsCorrectEndpointMethod()
            throws WeatherForecastException {
        // Arrange
        String latitude = "1.11";
        String longitude = "-1.11";
        ForecastType forecastType = ForecastType.DAILY;

        // Act
        this.processor.processWeatherForecastRequest(latitude, longitude, forecastType);

        // Assert
        verify(this.openWeatherMapEndpoints, times(1)).getDailyWeatherForecast(latitude, longitude);
    }
}
