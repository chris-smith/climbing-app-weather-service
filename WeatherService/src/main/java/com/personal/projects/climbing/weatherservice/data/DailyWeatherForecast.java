package com.personal.projects.climbing.weatherservice.data;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.personal.projects.climbing.weatherservice.util.WeatherUtils;

import java.sql.Timestamp;
import java.util.List;

/**
 * Stores the forecast details for a 24 hour period, as returned from OpenWeatherMap's 'One Call' API.
 *
 * @author Chris Smith
 */
public class DailyWeatherForecast {
    private Timestamp timeOfForecast;
    private Timestamp sunrise;
    private Timestamp sunset;
    private DailyTemp temperature;
    private DailyFeelsLikeTemp feelsLikeTemp;
    private double humidity;
    private double uvIndex;
    private double cloudiness;
    private double windSpeed;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private double windDirectionDegrees;
    private String windDirectionCompass;
    private double chanceOfRain;
    private List<WeatherOverview> weatherOverview;

    /**
     * @return the timestamp of the forecasted data, in UTC
     */
    @JsonGetter("timeOfForecast")
    public Timestamp getTimeOfForecast() {
        return this.timeOfForecast;
    }

    /**
     * @param timeOfForecast the timeOfForecast to set
     */
    @JsonSetter("dt")
    public void setTimeOfForecast(Long timeOfForecast) {
        this.timeOfForecast = new Timestamp(timeOfForecast * 1000);
    }

    /**
     * @return the forecasted sunrise timestamp
     */
    public Timestamp getSunrise() {
        return this.sunrise;
    }

    /**
     * @param sunrise the sunrise to set (in epoch seconds)
     */
    public void setSunrise(Long sunrise) {
        this.sunrise = new Timestamp(sunrise * 1000);
    }

    /**
     * @return the forecasted sunset timestamp
     */
    public Timestamp getSunset() {
        return this.sunset;
    }

    /**
     * @param sunset the sunset to set (in epoch seconds)
     */
    public void setSunset(Long sunset) {
        this.sunset = new Timestamp(sunset * 1000);
    }

    /**
     * @return the forecasted temperature info
     */
    @JsonGetter("temperature")
    public DailyTemp getTemperature() {
        return this.temperature;
    }

    /**
     * @param temperature the temperature to set
     */
    @JsonSetter("temp")
    public void setTemperature(DailyTemp temperature) {
        this.temperature = temperature;
    }

    /**
     * @return the forecasted feels-like temperature info
     */
    @JsonGetter("feelsLikeTemp")
    public DailyFeelsLikeTemp getFeelsLikeTemp() {
        return this.feelsLikeTemp;
    }

    /**
     * @param feelsLikeTemp the feelsLikeTemp to set
     */
    @JsonSetter("feels_like")
    public void setFeelsLikeTemp(DailyFeelsLikeTemp feelsLikeTemp) {
        this.feelsLikeTemp = feelsLikeTemp;
    }

    /**
     * @return the forecasted humidity, as a percentage
     */
    public double getHumidity() {
        return this.humidity;
    }

    /**
     * @param humidity the humidity to set
     */
    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    /**
     * @return the forecasted UV index
     */
    @JsonGetter("uvIndex")
    public double getUvIndex() {
        return this.uvIndex;
    }

    /**
     * @param uvIndex the uvIndex to set
     */
    @JsonSetter("uvi")
    public void setUvIndex(double uvIndex) {
        this.uvIndex = uvIndex;
    }

    /**
     * @return the forecasted cloudiness, as a percentage
     */
    @JsonGetter("cloudiness")
    public double getCloudiness() {
        return this.cloudiness;
    }

    /**
     * @param cloudiness the cloudiness to set
     */
    @JsonSetter("clouds")
    public void setCloudiness(double cloudiness) {
        this.cloudiness = cloudiness;
    }

    /**
     * @return the current wind speed, in miles per hour
     */
    @JsonGetter("windSpeed")
    public double getWindSpeed() {
        return this.windSpeed;
    }

    /**
     * @param windSpeed the windSpeed to set
     */
    @JsonSetter("wind_speed")
    public void setWindSpeed(double windSpeed) {
        this.windSpeed = windSpeed;
    }

    /**
     * @return the meteorological wind direction in degrees
     */
    public double getWindDirectionDegrees() {
        return this.windDirectionDegrees;
    }

    /**
     * @param windDirectionDegrees the windDirectionDegrees to set
     */
    @JsonSetter("wind_deg")
    public void setWindDirectionDegrees(double windDirectionDegrees) {
        this.windDirectionDegrees = windDirectionDegrees;
        this.setWindDirectionCompass(WeatherUtils.windDirectionDegreesToCompass(windDirectionDegrees));
    }

    /**
     * @return a series of characters representing the meteorological wind direction e.g. 'NW'
     */
    @JsonGetter("windDirection")
    public String getWindDirectionCompass() {
        return this.windDirectionCompass;
    }

    /**
     * @param windDirectionCompass the windDirectionCompass to set
     */
    public void setWindDirectionCompass(String windDirectionCompass) {
        this.windDirectionCompass = windDirectionCompass;
    }

    /**
     * @return the forecasted chance of rain, as a percentage
     */
    @JsonGetter("chanceOfRain")
    public double getChanceOfRain() {
        return this.chanceOfRain;
    }

    /**
     * @param chanceOfRain the chanceOfRain to set
     */
    @JsonSetter("pop")
    public void setChanceOfRain(double chanceOfRain) {
        this.chanceOfRain = chanceOfRain;
    }

    /**
     * @return an object containing the general weather forecast info
     */
    @JsonGetter("weatherOverview")
    public List<WeatherOverview> getWeatherOverview() {
        return this.weatherOverview;
    }

    /**
     * @param weatherOverview the weatherOverview to set
     */
    @JsonSetter("weather")
    public void setWeatherOverview(List<WeatherOverview> weatherOverview) {
        this.weatherOverview = weatherOverview;
    }
}
