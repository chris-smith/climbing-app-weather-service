package com.personal.projects.climbing.weatherservice.data;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

/**
 * Stores 'feels like' temperature info that is part of a daily weather forecast, as returned from
 * OpenWeatherMap's 'One Call' API.
 *
 * @author Chris Smith
 */
public class DailyFeelsLikeTemp {
    private double dayFeelsLikeTemp;
    private double nightFeelsLikeTemp;
    private double eveningFeelsLikeTemp;
    private double morningFeelsLikeTemp;

    /**
     * @return the forecasted feels-like temperature for the daytime, in degrees celsius
     */
    @JsonGetter("dayFeelsLikeTemp")
    public double getDayFeelsLikeTemp() {
        return this.dayFeelsLikeTemp;
    }

    /**
     * @param dayFeelsLikeTemp the dayTemp to set
     */
    @JsonSetter("day")
    public void setDayFeelsLikeTemp(double dayFeelsLikeTemp) {
        this.dayFeelsLikeTemp = dayFeelsLikeTemp;
    }

    /**
     * @return the forecasted feels-like temperature for the nighttime, in degrees celsius
     */
    @JsonGetter("nightFeelsLikeTemp")
    public double getNightFeelsLikeTemp() {
        return this.nightFeelsLikeTemp;
    }

    /**
     * @param nightFeelsLikeTemp the nightTemp to set
     */
    @JsonSetter("night")
    public void setNightFeelsLikeTemp(double nightFeelsLikeTemp) {
        this.nightFeelsLikeTemp = nightFeelsLikeTemp;
    }

    /**
     * @return the forecasted feels-like temperature for the evening, in degrees celsius
     */
    @JsonGetter("eveningFeelsLikeTemp")
    public double getEveningFeelsLikeTemp() {
        return this.eveningFeelsLikeTemp;
    }

    /**
     * @param eveningFeelsLikeTemp the eveningTemp to set
     */
    @JsonSetter("eve")
    public void setEveningFeelsLikeTemp(double eveningFeelsLikeTemp) {
        this.eveningFeelsLikeTemp = eveningFeelsLikeTemp;
    }

    /**
     * @return the forecasted feels-like temperature for the morning, in degrees celsius
     */
    @JsonGetter("morningFeelsLikeTemp")
    public double getMorningFeelsLikeTemp() {
        return this.morningFeelsLikeTemp;
    }

    /**
     * @param morningFeelsLikeTemp the morningTemp to set
     */
    @JsonSetter("morn")
    public void setMorningFeelsLikeTemp(double morningFeelsLikeTemp) {
        this.morningFeelsLikeTemp = morningFeelsLikeTemp;
    }
}
