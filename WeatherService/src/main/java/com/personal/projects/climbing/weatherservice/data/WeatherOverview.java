package com.personal.projects.climbing.weatherservice.data;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

/**
 * Stores some general information about a location's current weather, as part of an object returned from one of
 * OpenWeatherMap's APIs.
 *
 * @author Chris Smith
 */
public class WeatherOverview {
    private String condition;
    private String description;

    /**
     * @return a one-word description of the current weather
     */
    @JsonGetter("condition")
    public String getCondition() {
        return this.condition;
    }

    /**
     * @param condition the condition to set
     */
    @JsonSetter("main")
    public void setCondition(String condition) {
        this.condition = condition;
    }

    /**
     * @return a slightly more detailed description of the current weather
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
}
