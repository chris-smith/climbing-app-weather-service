package com.personal.projects.climbing.weatherservice.scheduling;

import com.personal.projects.climbing.weatherservice.processor.WeatherProcessor;
import com.personal.projects.climbing.weatherservice.rest.LocationServiceEndpoints;
import com.personal.projects.climbing.weatherservice.writer.WeatherUpdateWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Schedules {@link WeatherUpdateTask}s to run at regular intervals and stores a reference to each task, to enable
 * cancelling of tasks.
 *
 * @author Chris Smith
 */
@Component
public class WeatherUpdateScheduler {
    private static final Logger LOGGER = LoggerFactory.getLogger(WeatherUpdateScheduler.class);

    Map<Integer, ScheduledExecutorService> weatherUpdateTasks =
            new ConcurrentHashMap<Integer, ScheduledExecutorService>();

    @Autowired
    private LocationServiceEndpoints locationServiceEndpoints;

    @Autowired
    private WeatherProcessor processor;

    @Autowired
    private WeatherUpdateWriter writer;

    /**
     * Schedules a {@link WeatherUpdateTask} at the specified interval
     *
     * @param taskId          the unique identifier for the task/scheduled executor service that will be created - this
     *                        equates to the unique identifier of the user that the {@link WeatherUpdateTask}
     *                        corresponds to
     * @param intervalSeconds the interval at which the {@link WeatherUpdateTask} should run, in seconds
     */
    public void scheduleTask(int taskId, int intervalSeconds) {
        this.stopTask(taskId);
        LOGGER.info("Scheduling weather update task for user {} at an interval of {} seconds", taskId, intervalSeconds);
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        executor.scheduleAtFixedRate(
                new WeatherUpdateTask(taskId, this.locationServiceEndpoints, this.processor, this.writer),
                0,
                intervalSeconds,
                TimeUnit.SECONDS);
        this.weatherUpdateTasks.put(taskId, executor);
    }

    /**
     * Shuts down the {@link ScheduledExecutorService} (and hence all associated tasks) that has been stored in the
     * cache with the specified ID, if one exists
     *
     * @param id the ID of the {@link ScheduledExecutorService} (and hence all associated tasks) to stop
     */
    public void stopTask(int id) {
        if (this.weatherUpdateTasks.get(id) != null) {
            LOGGER.info("Stopping weather update task for user {}", id);
            ScheduledExecutorService existingExecutor = this.weatherUpdateTasks.get(id);
            if (existingExecutor != null) {
                existingExecutor.shutdown();
            }

            this.weatherUpdateTasks.remove(id);
        } else {
            LOGGER.info("Task already stopped.");
        }
    }
}
