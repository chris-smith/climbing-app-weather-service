package com.personal.projects.climbing.weatherservice.data;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

/**
 * Stores values for the main weather metrics for a location, as part of an overall 'Current Weather' object
 * returned from OpenWeatherMap's 'Current Weather' API.
 *
 * @author Chris Smith
 */
public class WeatherMetrics {
    private double temperature;
    private double feelsLikeTemp;
    private double minTemp;
    private double maxTemp;
    private double humidity;

    /**
     * @return the current temperature, in degrees celsius
     */
    @JsonGetter("temperature")
    public double getTemperature() {
        return this.temperature;
    }

    /**
     * @param temperature the temperature to set
     */
    @JsonSetter("temp")
    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    /**
     * @return the current feels-like temperature, in degrees celsius
     */
    @JsonGetter("feelsLikeTemp")
    public double getFeelsLikeTemp() {
        return this.feelsLikeTemp;
    }

    /**
     * @param feelsLikeTemp the feelsLikeTemp to set
     */
    @JsonSetter("feels_like")
    public void setFeelsLikeTemp(double feelsLikeTemp) {
        this.feelsLikeTemp = feelsLikeTemp;
    }

    /**
     * @return the minimum currently observed temperature (within large areas), in degrees celsius
     */
    @JsonGetter("minTemp")
    public double getMinTemp() {
        return this.minTemp;
    }

    /**
     * @param minTemp the minTemp to set
     */
    @JsonSetter("temp_min")
    public void setMinTemp(double minTemp) {
        this.minTemp = minTemp;
    }

    /**
     * @return the maximum currently observed temperature (within large areas), in degrees celsius
     */
    @JsonGetter("maxTemp")
    public double getMaxTemp() {
        return this.maxTemp;
    }

    /**
     * @param maxTemp the maxTemp to set
     */
    @JsonSetter("temp_max")
    public void setMaxTemp(double maxTemp) {
        this.maxTemp = maxTemp;
    }

    /**
     * @return the current humidity, as a percentage
     */
    public double getHumidity() {
        return humidity;
    }

    /**
     * @param humidity the humidity to set
     */
    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }
}
