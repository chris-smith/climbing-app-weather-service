package com.personal.projects.climbing.weatherservice.rest;

import com.personal.projects.climbing.weatherservice.data.ClimbingLocation;
import com.personal.projects.climbing.weatherservice.exception.LocationRetrievalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import java.util.Arrays;
import java.util.List;

/**
 * Class used for making REST calls to Location Service.
 *
 * @author Chris Smith
 */
@Component
public class LocationServiceEndpoints {
    private static final Logger LOGGER = LoggerFactory.getLogger(LocationServiceEndpoints.class);

    @Value("${location.service.outdoor-climbing-locations-url-template}")
    private String outdoorClimbingLocationsUrlTemplate;

    @Autowired
    private WebClient webClient;

    /**
     * Retrieves a user's saved outdoor climbing locations from Location Service
     *
     * @param userId the unique identifier of the user to retrieve climbing locations for
     * @return a list of {@link ClimbingLocation}s
     * @throws LocationRetrievalException if there is an error retrieving the locations from Location Service
     */
    public List<ClimbingLocation> getOutdoorClimbingLocations(int userId) throws LocationRetrievalException {
        String url = String.format(this.outdoorClimbingLocationsUrlTemplate, userId);
        LOGGER.debug("Making request to {} to retrieve user {}'s saved outdoor climbing locations", url, userId);

        try {
            ClimbingLocation[] response = this.webClient.get()
                    .uri(url)
                    .retrieve()
                    .bodyToMono(ClimbingLocation[].class)
                    .block();

            return Arrays.asList(response);
        } catch (WebClientResponseException e) {
            throw new LocationRetrievalException(e.getMessage(), e.getStatusCode(), e);
        }
    }
}
