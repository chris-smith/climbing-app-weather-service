package com.personal.projects.climbing.weatherservice.util;

import java.util.Arrays;
import java.util.List;

/**
 * Utility class for weather-related operations.
 *
 * @author Chris Smith
 */
public final class WeatherUtils {
    private static final List<String> WIND_DIRECTIONS_COMPASS = Arrays.asList(
            "N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW", "N");

    /**
     * Default private constructor for {@link WeatherUtils} to prevent instantiation
     */
    private WeatherUtils() {

    }

    /**
     * Converts the wind direction supplied in degrees to a series of characters representing the meteorological
     * wind direction e.g. 'NW'
     *
     * @param windDirectionDegrees the wind direction in degrees
     * @return the wind direction expressed as a series of characters
     */
    public static String windDirectionDegreesToCompass(double windDirectionDegrees) {
        return WIND_DIRECTIONS_COMPASS.get((int) Math.floor(((windDirectionDegrees + 11.25) % 360) / 22.5));
    }
}
