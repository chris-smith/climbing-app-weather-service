package com.personal.projects.climbing.weatherservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Entry point for the Weather Service application.
 *
 * @author Chris Smith
 */
@SpringBootApplication
public class WeatherService {
    /**
     * Entry point method for the {@link WeatherService}
     *
     * @param args string array of command line arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(WeatherService.class, args);
    }
}
