package com.personal.projects.climbing.weatherservice.enums;

/**
 * An enum that stores the different types of weather forecast that can be provided by the service.
 *
 * @author Chris Smith
 */
public enum ForecastType {
    HOURLY("hourly"),
    DAILY("daily");

    private final String value;

    /**
     * Constructor for creating an instance of {@link ForecastType}
     *
     * @param value the value to set
     */
    ForecastType(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value;
    }
}
