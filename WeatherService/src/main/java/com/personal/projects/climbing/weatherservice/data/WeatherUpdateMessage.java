package com.personal.projects.climbing.weatherservice.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.StdDateFormat;

import java.util.List;

/**
 * Represents a weather update message that is sent to Kafka, containing the current weather details for a number of
 * locations.
 *
 * @author Chris Smith
 */
public class WeatherUpdateMessage {
    private int userId;
    private List<WeatherUpdate> updates;

    /**
     * No-args constructor for serialisation/deserialisation
     */
    public WeatherUpdateMessage() {

    }

    /**
     * Constructor for creating an instance of {@link WeatherUpdateMessage}
     *
     * @param userId  the unique identifier of the user that this weather update message corresponds to
     * @param updates a list of {@link WeatherUpdate}s for each location
     */
    public WeatherUpdateMessage(int userId, List<WeatherUpdate> updates) {
        this.userId = userId;
        this.updates = updates;
    }

    /**
     * @return the unique identifier of the user that this weather update message corresponds to
     */
    public int getUserId() {
        return this.userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * @return a list of {@link WeatherUpdate}s for each location
     */
    public List<WeatherUpdate> getUpdates() {
        return this.updates;
    }

    /**
     * @param updates the updates to set
     */
    public void setUpdates(List<WeatherUpdate> updates) {
        this.updates = updates;
    }

    /**
     * Converts this {@link WeatherUpdateMessage} to a JSON string
     *
     * @return the JSON string
     * @throws JsonProcessingException on failure to convert to JSON
     */
    public String toJson() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.setDateFormat(new StdDateFormat().withColonInTimeZone(true));
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        return mapper.writeValueAsString(this);
    }
}
