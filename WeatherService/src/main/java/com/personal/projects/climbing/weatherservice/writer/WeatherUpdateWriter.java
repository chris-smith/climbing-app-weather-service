package com.personal.projects.climbing.weatherservice.writer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.personal.projects.climbing.weatherservice.data.WeatherUpdateMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

/**
 * Class used to write {@link WeatherUpdateMessage}s to the weather-updates Kafka topic.
 *
 * @author Chris Smith
 */
@Component
public class WeatherUpdateWriter {
    private static final Logger LOGGER = LoggerFactory.getLogger(WeatherUpdateWriter.class);

    @Value("${weather.updates.topic}")
    private String topic;

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    /**
     * Writes the supplied {@link WeatherUpdateMessage} to the weather-update Kafka topic
     *
     * @param weatherUpdate the {@link WeatherUpdateMessage} to send
     * @return a boolean indicating whether the message was sent successfully
     */
    public boolean sendWeatherUpdate(WeatherUpdateMessage weatherUpdate) {
        LOGGER.info("Writing weather update for user {} to the {} Kafka topic", weatherUpdate.getUserId(), this.topic);
        try {
            this.kafkaTemplate.send(this.topic, weatherUpdate.toJson());
            return true;
        } catch (JsonProcessingException e) {
            LOGGER.error("Failed to convert weather update for user {} to JSON. Will not be retried.",
                    weatherUpdate.getUserId(), e);
            return false;
        }
    }
}
