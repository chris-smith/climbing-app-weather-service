package com.personal.projects.climbing.weatherservice.config;

import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.reactive.ClientHttpConnector;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;

import java.util.concurrent.TimeUnit;

/**
 * Configures a {@link WebClient} to be used throughout the service for REST communications.
 *
 * @author Chris Smith
 */
@Configuration
public class WebClientConfig {
    @Value("${rest.web.client.timeout-milliseconds}")
    private int TIMEOUT;

    /**
     * Creates and configures a {@link WebClient} instance to be used throughout the service
     *
     * @return the {@link WebClient}
     */
    @Bean
    public WebClient configureWebClient() {
        HttpClient httpClient = HttpClient.create()
                .tcpConfiguration(tcpClient -> {
                    tcpClient = tcpClient.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, this.TIMEOUT);
                    tcpClient = tcpClient.doOnConnected(conn -> conn
                            .addHandlerLast(new ReadTimeoutHandler(this.TIMEOUT, TimeUnit.MILLISECONDS)));
                    return tcpClient;
                });

        ClientHttpConnector connector = new ReactorClientHttpConnector(httpClient);
        return WebClient.builder().clientConnector(connector).build();
    }
}
