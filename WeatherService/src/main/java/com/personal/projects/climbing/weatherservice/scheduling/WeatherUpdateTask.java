package com.personal.projects.climbing.weatherservice.scheduling;

import com.personal.projects.climbing.weatherservice.data.ClimbingLocation;
import com.personal.projects.climbing.weatherservice.data.CurrentWeather;
import com.personal.projects.climbing.weatherservice.data.WeatherUpdate;
import com.personal.projects.climbing.weatherservice.data.WeatherUpdateMessage;
import com.personal.projects.climbing.weatherservice.exception.CurrentWeatherException;
import com.personal.projects.climbing.weatherservice.exception.LocationRetrievalException;
import com.personal.projects.climbing.weatherservice.processor.WeatherProcessor;
import com.personal.projects.climbing.weatherservice.rest.LocationServiceEndpoints;
import com.personal.projects.climbing.weatherservice.writer.WeatherUpdateWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Retrieves current weather data at regular intervals and writes it to a Kafka topic, for consumption by the UI.
 *
 * @author Chris Smith
 */
public class WeatherUpdateTask implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(WeatherUpdateTask.class);

    private int userId;
    private LocationServiceEndpoints locationServiceEndpoints;
    private WeatherProcessor processor;
    private WeatherUpdateWriter writer;

    /**
     * Constructor for creating an instance of {@link WeatherUpdateTask}
     *
     * @param userId                   the unique identifier of the user that the {@link WeatherUpdateMessage}
     *                                 corresponds to
     * @param locationServiceEndpoints the {@link LocationServiceEndpoints} instance to use to retrieve the locations
     *                                 that should be included in the {@link WeatherUpdateMessage}
     * @param processor                the {@link WeatherProcessor} instance to use to get the current weather for each
     *                                 location
     * @param writer                   the {@link WeatherUpdateWriter} to use to send the
     *                                 {@link WeatherUpdateMessage}s to Kafka
     */
    public WeatherUpdateTask(
            int userId,
            LocationServiceEndpoints locationServiceEndpoints,
            WeatherProcessor processor,
            WeatherUpdateWriter writer) {
        this.userId = userId;
        this.locationServiceEndpoints = locationServiceEndpoints;
        this.processor = processor;
        this.writer = writer;
    }

    @Override
    public void run() {
        LOGGER.debug("Running weather update task for user {}", this.userId);
        List<ClimbingLocation> climbingLocations = null;
        try {
            climbingLocations = this.locationServiceEndpoints.getOutdoorClimbingLocations(this.userId);
        } catch (LocationRetrievalException e) {
            LOGGER.error("Error retrieving locations from Location Service - this run of the weather update task will" +
                    " be skipped. \nError: ", e);
            return;
        }
        List<WeatherUpdate> weatherUpdates = new ArrayList<WeatherUpdate>();
        for (ClimbingLocation location : climbingLocations) {
            try {
                CurrentWeather currentWeather = this.processor.processCurrentWeatherRequest(
                        String.valueOf(location.getLatitude()),
                        String.valueOf(location.getLongitude()));

                WeatherUpdate weatherUpdate = new WeatherUpdate(location, currentWeather);
                weatherUpdates.add(weatherUpdate);
            } catch (CurrentWeatherException e) {
                LOGGER.error("Error retrieving current weather for {}, {}. Skipping this location for this update.",
                        location.getPostCode(), location.getCountry());
            }
        }

        WeatherUpdateMessage weatherUpdateMessage = new WeatherUpdateMessage(this.userId, weatherUpdates);
        if (this.writer.sendWeatherUpdate(weatherUpdateMessage)) {
            LOGGER.debug("Successfully completed weather update task for user {}", this.userId);
        } else {
            LOGGER.error("Error sending weather update for user {}. Will be retried next run of the task.",
                    this.userId);
        }
    }
}
