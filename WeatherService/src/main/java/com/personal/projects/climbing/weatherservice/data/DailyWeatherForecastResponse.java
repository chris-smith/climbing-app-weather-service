package com.personal.projects.climbing.weatherservice.data;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.util.List;

/**
 * Object representation of the JSON returned from OpenWeatherMap's 'One Call' API, that is used to retrieve
 * a daily forecast for the next 7 days.
 *
 * @author Chris Smith
 */
public class DailyWeatherForecastResponse extends WeatherForecast {
    private List<DailyWeatherForecast> results;

    /**
     * @return the list of daily forecast results
     */
    @JsonGetter("results")
    public List<DailyWeatherForecast> getResults() {
        return this.results;
    }

    /**
     * @param results the results to set
     */
    @JsonSetter("daily")
    public void setResults(List<DailyWeatherForecast> results) {
        this.results = results;
    }
}
