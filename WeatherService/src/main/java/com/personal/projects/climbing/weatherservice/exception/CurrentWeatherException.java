package com.personal.projects.climbing.weatherservice.exception;

import org.springframework.http.HttpStatus;

/**
 * An exception that is thrown when there is an error requesting current weather data from OpenWeatherMap's
 * 'Current Weather' API.
 *
 * @author Chris Smith
 */
public class CurrentWeatherException extends Exception {
    private HttpStatus statusCode;

    /**
     * Constructor for creating an instance of {@link CurrentWeatherException}
     */
    public CurrentWeatherException() {

    }

    /**
     * Constructor for creating an instance of {@link CurrentWeatherException}
     *
     * @param message    the exception message detail
     * @param statusCode the {@link HttpStatus} of the response that caused this exception
     * @param cause      the cause of the exception
     */
    public CurrentWeatherException(String message, HttpStatus statusCode, Throwable cause) {
        super(message, cause);
        this.statusCode = statusCode;
    }

    /**
     * @return the {@link HttpStatus} of the response that caused this exception
     */
    public HttpStatus getStatusCode() {
        return this.statusCode;
    }
}
