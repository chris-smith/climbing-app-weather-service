package com.personal.projects.climbing.weatherservice.data;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.util.List;

/**
 * Object representation of the JSON returned from OpenWeatherMap's 'Current Weather' API for a particular location.
 *
 * @author Chris Smith
 */
public class CurrentWeather {
    private Coordinates coordinates;
    private List<WeatherOverview> weatherOverview;
    private WeatherMetrics weatherMetrics;
    private int visibility;
    private WindInfo windInfo;
    private DaylightInfo daylightInfo;

    /**
     * @return the {@link Coordinates} of the location that this current weather object corresponds to
     */
    @JsonGetter("coordinates")
    public Coordinates getCoordinates() {
        return this.coordinates;
    }

    /**
     * @param coordinates the coordinates to set
     */
    @JsonSetter("coord")
    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    /**
     * @return the general weather info related to this current weather object
     */
    @JsonGetter("weatherOverview")
    public List<WeatherOverview> getWeatherOverview() {
        return this.weatherOverview;
    }

    /**
     * @param weatherOverview the weatherOverview to set
     */
    @JsonSetter("weather")
    public void setWeatherOverview(List<WeatherOverview> weatherOverview) {
        this.weatherOverview = weatherOverview;
    }

    /**
     * @return the main weather metrics of this current weather object e.g. temperature and humidity
     */
    @JsonGetter("weatherMetrics")
    public WeatherMetrics getWeatherMetrics() {
        return this.weatherMetrics;
    }

    /**
     * @param weatherMetrics the weatherMetrics to set
     */
    @JsonSetter("main")
    public void setWeatherMetrics(WeatherMetrics weatherMetrics) {
        this.weatherMetrics = weatherMetrics;
    }

    /**
     * @return the current visibility, in metres
     */
    public int getVisibility() {
        return visibility;
    }

    /**
     * @param visibility the visibility to set
     */
    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    /**
     * @return the wind-related information of this current weather object
     */
    @JsonGetter("windInfo")
    public WindInfo getWindInfo() {
        return this.windInfo;
    }

    /**
     * @param windInfo the windInfo to set
     */
    @JsonSetter("wind")
    public void setWindInfo(WindInfo windInfo) {
        this.windInfo = windInfo;
    }

    /**
     * @return the daylight-related information of this current weather object
     */
    @JsonGetter("daylightInfo")
    public DaylightInfo getDaylightInfo() {
        return this.daylightInfo;
    }

    /**
     * @param daylightInfo the daylightInfo to set
     */
    @JsonSetter("sys")
    public void setDaylightInfo(DaylightInfo daylightInfo) {
        this.daylightInfo = daylightInfo;
    }
}
