package com.personal.projects.climbing.weatherservice.data;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.personal.projects.climbing.weatherservice.util.WeatherUtils;

import java.sql.Timestamp;
import java.util.List;

/**
 * Stores the forecast details for a one hour period, as returned from OpenWeatherMap's 'One Call' API.
 *
 * @author Chris Smith
 */
public class HourlyWeatherForecast {
    private Timestamp timeOfForecast;
    private double temperature;
    private double feelsLikeTemp;
    private double humidity;
    private double uvIndex;
    private double cloudiness;
    private double visibility;
    private double windSpeed;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private double windDirectionDegrees;
    private String windDirectionCompass;
    private double chanceOfRain;
    private List<WeatherOverview> weatherOverview;

    /**
     * @return the timestamp of the forecasted data, in UTC
     */
    @JsonGetter("timeOfForecast")
    public Timestamp getTimeOfForecast() {
        return this.timeOfForecast;
    }

    /**
     * @param timeOfForecast the timeOfForecast to set
     */
    @JsonSetter("dt")
    public void setTimeOfForecast(Long timeOfForecast) {
        this.timeOfForecast = new Timestamp(timeOfForecast * 1000);
    }

    /**
     * @return the forecasted temperature, in degrees celsius
     */
    @JsonGetter("temperature")
    public double getTemperature() {
        return this.temperature;
    }

    /**
     * @param temperature the temperature to set
     */
    @JsonSetter("temp")
    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    /**
     * @return the forecasted feels-like temperature, in degrees celsius
     */
    @JsonGetter("feelsLikeTemp")
    public double getFeelsLikeTemp() {
        return this.feelsLikeTemp;
    }

    /**
     * @param feelsLikeTemp the feelsLikeTemp to set
     */
    @JsonSetter("feels_like")
    public void setFeelsLikeTemp(double feelsLikeTemp) {
        this.feelsLikeTemp = feelsLikeTemp;
    }

    /**
     * @return the forecasted humidity, as a percentage
     */
    public double getHumidity() {
        return this.humidity;
    }

    /**
     * @param humidity the humidity to set
     */
    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    /**
     * @return the forecasted UV index
     */
    @JsonGetter("uvIndex")
    public double getUvIndex() {
        return this.uvIndex;
    }

    /**
     * @param uvIndex the uvIndex to set
     */
    @JsonSetter("uvi")
    public void setUvIndex(double uvIndex) {
        this.uvIndex = uvIndex;
    }

    /**
     * @return the forecasted cloudiness, as a percentage
     */
    @JsonGetter("cloudiness")
    public double getCloudiness() {
        return this.cloudiness;
    }

    /**
     * @param cloudiness the cloudiness to set
     */
    @JsonSetter("clouds")
    public void setCloudiness(double cloudiness) {
        this.cloudiness = cloudiness;
    }

    /**
     * @return the forecasted average visibility, in metres
     */
    public double getVisibility() {
        return this.visibility;
    }

    /**
     * @param visibility the visibility to set
     */
    public void setVisibility(double visibility) {
        this.visibility = visibility;
    }

    /**
     * @return the current wind speed, in miles per hour
     */
    @JsonGetter("windSpeed")
    public double getWindSpeed() {
        return this.windSpeed;
    }

    /**
     * @param windSpeed the windSpeed to set
     */
    @JsonSetter("wind_speed")
    public void setWindSpeed(double windSpeed) {
        this.windSpeed = windSpeed;
    }

    /**
     * @return the meteorological wind direction in degrees
     */
    public double getWindDirectionDegrees() {
        return this.windDirectionDegrees;
    }

    /**
     * @param windDirectionDegrees the windDirectionDegrees to set
     */
    @JsonSetter("wind_deg")
    public void setWindDirectionDegrees(double windDirectionDegrees) {
        this.windDirectionDegrees = windDirectionDegrees;
        this.setWindDirectionCompass(WeatherUtils.windDirectionDegreesToCompass(windDirectionDegrees));
    }

    /**
     * @return a series of characters representing the meteorological wind direction e.g. 'NW'
     */
    @JsonGetter("windDirection")
    public String getWindDirectionCompass() {
        return this.windDirectionCompass;
    }

    /**
     * @param windDirectionCompass the windDirectionCompass to set
     */
    public void setWindDirectionCompass(String windDirectionCompass) {
        this.windDirectionCompass = windDirectionCompass;
    }

    /**
     * @return the forecasted chance of rain, as a percentage
     */
    @JsonGetter("chanceOfRain")
    public double getChanceOfRain() {
        return this.chanceOfRain;
    }

    /**
     * @param chanceOfRain the chanceOfRain to set
     */
    @JsonSetter("pop")
    public void setChanceOfRain(double chanceOfRain) {
        this.chanceOfRain = chanceOfRain;
    }

    /**
     * @return an object containing the general weather forecast info
     */
    @JsonGetter("weatherOverview")
    public List<WeatherOverview> getWeatherOverview() {
        return this.weatherOverview;
    }

    /**
     * @param weatherOverview the weatherOverview to set
     */
    @JsonSetter("weather")
    public void setWeatherOverview(List<WeatherOverview> weatherOverview) {
        this.weatherOverview = weatherOverview;
    }
}
