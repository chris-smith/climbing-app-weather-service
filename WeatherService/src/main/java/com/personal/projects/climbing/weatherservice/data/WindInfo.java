package com.personal.projects.climbing.weatherservice.data;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.personal.projects.climbing.weatherservice.util.WeatherUtils;

/**
 * Stores info related to the wind condition, as part of an overall 'Current Weather' object returned from
 * OpenWeatherMap's 'Current Weather' API.
 *
 * @author Chris Smith
 */
public class WindInfo {
    private double windSpeed;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private double windDirectionDegrees;
    private String windDirectionCompass;

    /**
     * @return the current wind speed, in miles per hour
     */
    @JsonGetter("windSpeed")
    public double getWindSpeed() {
        return this.windSpeed;
    }

    /**
     * @param windSpeed the windSpeed to set
     */
    @JsonSetter("speed")
    public void setWindSpeed(double windSpeed) {
        this.windSpeed = windSpeed;
    }

    /**
     * @return the meteorological wind direction in degrees
     */
    public double getWindDirectionDegrees() {
        return this.windDirectionDegrees;
    }

    /**
     * @param windDirectionDegrees the windDirectionDegrees to set
     */
    @JsonSetter("deg")
    public void setWindDirectionDegrees(double windDirectionDegrees) {
        this.windDirectionDegrees = windDirectionDegrees;
        this.setWindDirectionCompass(WeatherUtils.windDirectionDegreesToCompass(windDirectionDegrees));
    }

    /**
     * @return a series of characters representing the meteorological wind direction e.g. 'NW'
     */
    @JsonGetter("windDirection")
    public String getWindDirectionCompass() {
        return this.windDirectionCompass;
    }

    /**
     * @param windDirectionCompass the windDirectionCompass to set
     */
    public void setWindDirectionCompass(String windDirectionCompass) {
        this.windDirectionCompass = windDirectionCompass;
    }
}
