package com.personal.projects.climbing.weatherservice.data;

import java.sql.Timestamp;

/**
 * Stores daylight-related info, as part of an overall 'Current Weather' object returned from OpenWeatherMap's
 * 'Current Weather' API.
 *
 * @author Chris Smith
 */
public class DaylightInfo {
    private Timestamp sunrise;
    private Timestamp sunset;

    /**
     * @return the timestamp at which sunrise occurred/will occur on the current day
     */
    public Timestamp getSunrise() {
        return this.sunrise;
    }

    /**
     * @param sunrise the sunrise to set (in epoch seconds)
     */
    public void setSunrise(Long sunrise) {
        this.sunrise = new Timestamp(sunrise * 1000);
    }

    /**
     * @return the timestamp at which sunset occurred/will occur on the current day
     */
    public Timestamp getSunset() {
        return this.sunset;
    }

    /**
     * @param sunset the sunset to set (in epoch seconds)
     */
    public void setSunset(Long sunset) {
        this.sunset = new Timestamp(sunset * 1000);
    }
}
