package com.personal.projects.climbing.weatherservice.data;

/**
 * Represents a weather update for a specific location.
 *
 * @author Chris Smith
 */
public class WeatherUpdate {
    private ClimbingLocation location;
    private CurrentWeather currentWeather;

    /**
     * No-args constructor for serialisation/deserialisation
     */
    public WeatherUpdate() {

    }

    /**
     * Constructor for creating an instance of {@link WeatherUpdate}
     *
     * @param climbingLocation information about the climbing location that this {@link WeatherUpdate}
     *                         corresponds to
     * @param currentWeather   the {@link CurrentWeather} for this location
     */
    public WeatherUpdate(ClimbingLocation climbingLocation, CurrentWeather currentWeather) {
        this.location = climbingLocation;
        this.currentWeather = currentWeather;
    }

    /**
     * @return information about the location that this {@link WeatherUpdate} corresponds to
     */
    public ClimbingLocation getLocation() {
        return this.location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(ClimbingLocation location) {
        this.location = location;
    }

    /**
     * @return the {@link CurrentWeather} for this location
     */
    public CurrentWeather getCurrentWeather() {
        return this.currentWeather;
    }

    /**
     * @param currentWeather the currentWeather to set
     */
    public void setCurrentWeather(CurrentWeather currentWeather) {
        this.currentWeather = currentWeather;
    }
}
