package com.personal.projects.climbing.weatherservice.data;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.util.List;

/**
 * Object representation of the JSON returned from OpenWeatherMap's 'One Call' API, that is used to retrieve
 * an hourly forecast covering the next 48 hours.
 *
 * @author Chris Smith
 */
public class HourlyWeatherForecastResponse extends WeatherForecast {
    private List<HourlyWeatherForecast> results;

    /**
     * @return the list of hourly forecast results
     */
    @JsonGetter("results")
    public List<HourlyWeatherForecast> getResults() {
        return this.results;
    }

    /**
     * @param results the results to set
     */
    @JsonSetter("hourly")
    public void setResults(List<HourlyWeatherForecast> results) {
        this.results = results;
    }
}
