package com.personal.projects.climbing.weatherservice.data;

import javax.validation.constraints.Positive;

/**
 * Contains details of a request for a new weather update task to be created/started.
 *
 * @author Chris Smith
 */
public class WeatherUpdateRequest {
    @Positive
    private int userId;
    @Positive
    private int intervalSeconds;

    /**
     * @return the unique identifier of the user that this weather update task is to be created for
     */
    public int getUserId() {
        return this.userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * @return the interval at which the weather update task should run, in seconds
     */
    public int getIntervalSeconds() {
        return this.intervalSeconds;
    }

    /**
     * @param intervalSeconds the intervalSeconds to set
     */
    public void setIntervalSeconds(int intervalSeconds) {
        this.intervalSeconds = intervalSeconds;
    }
}
