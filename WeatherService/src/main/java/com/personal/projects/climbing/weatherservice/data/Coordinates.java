package com.personal.projects.climbing.weatherservice.data;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

/**
 * Stores the geographical coordinates of a location, as part of an overall 'Current Weather' object returned from
 * OpenWeatherMap's 'Current Weather' API.
 *
 * @author Chris Smith
 */
public class Coordinates {
    private double longitude;
    private double latitude;

    /**
     * @return the longitude of the location's geo location
     */
    @JsonGetter("longitude")
    public double getLongitude() {
        return this.longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    @JsonSetter("lon")
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    /**
     * @return the latitude of the location's geo location
     */
    @JsonGetter("latitude")
    public double getLatitude() {
        return this.latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    @JsonSetter("lat")
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
}
