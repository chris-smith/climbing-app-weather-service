package com.personal.projects.climbing.weatherservice.rest;

import com.personal.projects.climbing.weatherservice.data.CurrentWeather;
import com.personal.projects.climbing.weatherservice.data.DailyWeatherForecastResponse;
import com.personal.projects.climbing.weatherservice.data.HourlyWeatherForecastResponse;
import com.personal.projects.climbing.weatherservice.exception.CurrentWeatherException;
import com.personal.projects.climbing.weatherservice.exception.WeatherForecastException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;

/**
 * Class used for making REST calls to OpenWeatherMap's APIs. See https://openweathermap.org/current for more info.
 *
 * @author Chris Smith
 */
@Component
public class OpenWeatherMapEndpoints {
    private static final Logger LOGGER = LoggerFactory.getLogger(OpenWeatherMapEndpoints.class);

    @Value("${open.weather.map.current-weather-url-template}")
    private String currentWeatherUrlTemplate;

    @Value("${open.weather.map.two-day.hourly-forecast-url-template}")
    private String hourlyForecastUrlTemplate;

    @Value("${open.weather.map.seven-day.daily-forecast-url-template}")
    private String dailyForecastUrlTemplate;

    @Autowired
    private WebClient webClient;

    /**
     * Makes a request to OpenWeatherMap's 'Current Weather' API to retrieve the {@link CurrentWeather} for the
     * specified latitude and longitude
     *
     * @param latitude  the latitude of the location to retrieve the current weather for
     * @param longitude the longitude of the location to retrieve the current weather for
     * @return a {@link CurrentWeather} object storing the details retrieved from the API
     * @throws CurrentWeatherException on failure to retrieve valid response from OWM
     */
    public CurrentWeather getCurrentWeather(String latitude, String longitude)
            throws CurrentWeatherException {
        String url = String.format(this.currentWeatherUrlTemplate, latitude, longitude);
        LOGGER.debug("Making request to {} to retrieve the current weather for latitude {}, longitude {}",
                url, latitude, longitude);

        try {
            return this.webClient.get()
                    .uri(url)
                    .retrieve()
                    .bodyToMono(CurrentWeather.class)
                    .block();
        } catch (WebClientResponseException e) {
            throw new CurrentWeatherException(e.getMessage(), e.getStatusCode(), e);
        }
    }

    /**
     * Makes a request to OpenWeatherMap's 'One Call' API to retrieve an hourly weather forecast for the next 48 hours,
     * for the specified location
     *
     * @param latitude  the latitude of the location to get the hourly forecast for
     * @param longitude the longitude of the location to get the hourly forecast for
     * @return an {@link HourlyWeatherForecastResponse} object storing the details retrieved from the API
     * @throws WeatherForecastException on failure to retrieve valid response from OWM
     */
    public HourlyWeatherForecastResponse getHourlyWeatherForecast(String latitude, String longitude)
            throws WeatherForecastException {
        String url = String.format(this.hourlyForecastUrlTemplate, latitude, longitude);
        LOGGER.debug("Making request to {} to retrieve an hourly forecast for latitude {}, longitude {}",
                url, latitude, longitude);

        try {
            return this.webClient.get()
                    .uri(url)
                    .retrieve()
                    .bodyToMono(HourlyWeatherForecastResponse.class)
                    .block();
        } catch (WebClientResponseException e) {
            throw new WeatherForecastException(e.getMessage(), e.getStatusCode(), e);
        }
    }

    /**
     * Makes a request to OpenWeatherMap's 'One Call' API to retrieve a daily weather forecast for the next 7 days,
     * for the specified location
     *
     * @param latitude  the latitude of the location to get the daily forecast for
     * @param longitude the longitude of the location to get the daily forecast for
     * @return a {@link DailyWeatherForecastResponse} object storing the details retrieved from the API
     * @throws WeatherForecastException on failure to retrieve valid response from OWM
     */
    public DailyWeatherForecastResponse getDailyWeatherForecast(String latitude, String longitude)
            throws WeatherForecastException {
        String url = String.format(this.dailyForecastUrlTemplate, latitude, longitude);
        LOGGER.debug("Making request to {} to retrieve a daily forecast for latitude {}, longitude {}",
                url, latitude, longitude);

        try {
            return this.webClient.get()
                    .uri(url)
                    .retrieve()
                    .bodyToMono(DailyWeatherForecastResponse.class)
                    .block();
        } catch (WebClientResponseException e) {
            throw new WeatherForecastException(e.getMessage(), e.getStatusCode(), e);
        }
    }
}
