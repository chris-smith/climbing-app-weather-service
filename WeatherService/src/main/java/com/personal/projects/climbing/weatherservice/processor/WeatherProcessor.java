package com.personal.projects.climbing.weatherservice.processor;

import com.personal.projects.climbing.weatherservice.data.CurrentWeather;
import com.personal.projects.climbing.weatherservice.data.WeatherForecast;
import com.personal.projects.climbing.weatherservice.enums.ForecastType;
import com.personal.projects.climbing.weatherservice.exception.CurrentWeatherException;
import com.personal.projects.climbing.weatherservice.exception.WeatherForecastException;
import com.personal.projects.climbing.weatherservice.rest.OpenWeatherMapEndpoints;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Processor class for weather-related operations.
 *
 * @author Chris Smith
 */
@Component
public class WeatherProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(WeatherProcessor.class);

    @Autowired
    private OpenWeatherMapEndpoints openWeatherMapEndpoints;

    /**
     * Processes a request to retrieve the current weather for the specified location
     *
     * @param latitude  the latitude of the location to get the current weather for
     * @param longitude the longitude of the location to get the current weather for
     * @return a {@link CurrentWeather} object populated for the specified location
     * @throws CurrentWeatherException on failure to retrieve valid response from OWM
     */
    public CurrentWeather processCurrentWeatherRequest(String latitude, String longitude)
            throws CurrentWeatherException {
        return this.openWeatherMapEndpoints.getCurrentWeather(latitude, longitude);
    }

    /**
     * Processes a request to retrieve a weather forecast for the specified location
     *
     * @param latitude     the latitude of the location to get the weather forecast for
     * @param longitude    the longitude of the location to get the weather forecast for
     * @param forecastType the type of forecast to retrieve e.g. hourly
     * @return a {@link WeatherForecast} object populated for the specified location
     * @throws WeatherForecastException on failure to retrieve valid response from OWM
     */
    public WeatherForecast processWeatherForecastRequest(String latitude, String longitude, ForecastType forecastType)
            throws WeatherForecastException {
        if (forecastType == ForecastType.HOURLY) {
            return this.openWeatherMapEndpoints.getHourlyWeatherForecast(latitude, longitude);
        }

        return this.openWeatherMapEndpoints.getDailyWeatherForecast(latitude, longitude);
    }
}
