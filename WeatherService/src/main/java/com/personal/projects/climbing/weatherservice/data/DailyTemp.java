package com.personal.projects.climbing.weatherservice.data;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

/**
 * Stores temperature info that is part of a daily weather forecast, as returned from OpenWeatherMap's 'One Call' API.
 *
 * @author Chris Smith
 */
public class DailyTemp {
    private double dayTemp;
    private double minTemp;
    private double maxTemp;
    private double nightTemp;
    private double eveningTemp;
    private double morningTemp;

    /**
     * @return the forecasted temperature for the daytime, in degrees celsius
     */
    @JsonGetter("dayTemp")
    public double getDayTemp() {
        return this.dayTemp;
    }

    /**
     * @param dayTemp the dayTemp to set
     */
    @JsonSetter("day")
    public void setDayTemp(double dayTemp) {
        this.dayTemp = dayTemp;
    }

    /**
     * @return the forecasted minimum temperature, in degrees celsius
     */
    @JsonGetter("minTemp")
    public double getMinTemp() {
        return this.minTemp;
    }

    /**
     * @param minTemp the minTemp to set
     */
    @JsonSetter("min")
    public void setMinTemp(double minTemp) {
        this.minTemp = minTemp;
    }

    /**
     * @return the forecasted maximum temperature, in degrees celsius
     */
    @JsonGetter("maxTemp")
    public double getMaxTemp() {
        return this.maxTemp;
    }

    /**
     * @param maxTemp the maxTemp to set
     */
    @JsonSetter("max")
    public void setMaxTemp(double maxTemp) {
        this.maxTemp = maxTemp;
    }

    /**
     * @return the forecasted temperature for the nighttime, in degrees celsius
     */
    @JsonGetter("nightTemp")
    public double getNightTemp() {
        return this.nightTemp;
    }

    /**
     * @param nightTemp the nightTemp to set
     */
    @JsonSetter("night")
    public void setNightTemp(double nightTemp) {
        this.nightTemp = nightTemp;
    }

    /**
     * @return the forecasted temperature for the evening, in degrees celsius
     */
    @JsonGetter("eveningTemp")
    public double getEveningTemp() {
        return this.eveningTemp;
    }

    /**
     * @param eveningTemp the eveningTemp to set
     */
    @JsonSetter("eve")
    public void setEveningTemp(double eveningTemp) {
        this.eveningTemp = eveningTemp;
    }

    /**
     * @return the forecasted temperature for the morning, in degrees celsius
     */
    @JsonGetter("morningTemp")
    public double getMorningTemp() {
        return this.morningTemp;
    }

    /**
     * @param morningTemp the morningTemp to set
     */
    @JsonSetter("morn")
    public void setMorningTemp(double morningTemp) {
        this.morningTemp = morningTemp;
    }
}
