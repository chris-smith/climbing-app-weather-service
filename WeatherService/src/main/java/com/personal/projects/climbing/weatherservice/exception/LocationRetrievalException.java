package com.personal.projects.climbing.weatherservice.exception;

import org.springframework.http.HttpStatus;

/**
 * An exception that is thrown when there is an error retrieving locations from the Location Service.
 *
 * @author Chris Smith
 */
public class LocationRetrievalException extends Exception {
    private HttpStatus statusCode;

    /**
     * Constructor for creating an instance of {@link LocationRetrievalException}
     */
    public LocationRetrievalException() {

    }

    /**
     * Constructor for creating an instance of {@link LocationRetrievalException}
     *
     * @param message    the exception message detail
     * @param statusCode the {@link HttpStatus} of the response that caused this exception
     * @param cause      the cause of the exception
     */
    public LocationRetrievalException(String message, HttpStatus statusCode, Throwable cause) {
        super(message, cause);
        this.statusCode = statusCode;
    }

    /**
     * @return the {@link HttpStatus} of the response that caused this exception
     */
    public HttpStatus getStatusCode() {
        return this.statusCode;
    }
}
