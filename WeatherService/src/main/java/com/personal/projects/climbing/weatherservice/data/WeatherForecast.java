package com.personal.projects.climbing.weatherservice.data;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

/**
 * Base class for the weather forecast-related JSON responses from OpenWeatherMap's 'One Call' weather forecast API.
 *
 * @author Chris Smith
 */
public abstract class WeatherForecast {
    private double longitude;
    private double latitude;
    private String timezone;
    private int timezoneOffset;

    /**
     * @return the longitude of the location's geo location
     */
    @JsonGetter("longitude")
    public double getLongitude() {
        return this.longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    @JsonSetter("lon")
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    /**
     * @return the latitude of the location's geo location
     */
    @JsonGetter("latitude")
    public double getLatitude() {
        return this.latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    @JsonSetter("lat")
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the timezone name for the location
     */
    public String getTimezone() {
        return this.timezone;
    }

    /**
     * @param timezone the timezone to set
     */
    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    /**
     * @return the timezone offset from UTC, in seconds
     */
    @JsonGetter("timezoneOffset")
    public int getTimezoneOffset() {
        return this.timezoneOffset;
    }

    /**
     * @param timezoneOffset the timezoneOffset to set
     */
    @JsonSetter("timezone_offset")
    public void setTimezoneOffset(int timezoneOffset) {
        this.timezoneOffset = timezoneOffset;
    }
}
