package com.personal.projects.climbing.weatherservice.rest;

import com.personal.projects.climbing.weatherservice.data.CurrentWeather;
import com.personal.projects.climbing.weatherservice.data.DailyWeatherForecastResponse;
import com.personal.projects.climbing.weatherservice.data.HourlyWeatherForecastResponse;
import com.personal.projects.climbing.weatherservice.data.WeatherUpdateRequest;
import com.personal.projects.climbing.weatherservice.enums.ForecastType;
import com.personal.projects.climbing.weatherservice.exception.CurrentWeatherException;
import com.personal.projects.climbing.weatherservice.exception.WeatherForecastException;
import com.personal.projects.climbing.weatherservice.processor.WeatherProcessor;
import com.personal.projects.climbing.weatherservice.scheduling.WeatherUpdateScheduler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * A controller that exposes REST APIs for retrieving weather-related data.
 *
 * @author Chris Smith
 */
@RestController
public class WeatherController {
    private static final Logger LOGGER = LoggerFactory.getLogger(WeatherController.class);

    @Autowired
    private WeatherProcessor processor;

    @Autowired
    private WeatherUpdateScheduler weatherUpdateScheduler;

    /**
     * REST API for getting the current weather at the specified location
     *
     * @param latitude  the latitude of the location to get the current weather for
     * @param longitude the longitude of the location to get the current weather for
     * @return a {@link CurrentWeather} object in JSON format
     */
    @GetMapping(path = "/current-weather", produces = "application/json")
    public ResponseEntity<?> getCurrentWeather(
            @RequestParam(name = "latitude", required = true) String latitude,
            @RequestParam(name = "longitude", required = true) String longitude) {
        LOGGER.info("Received request at /current-weather endpoint. Latitude: {}. Longitude: {}.", latitude, longitude);

        CurrentWeather currentWeather = null;
        try {
            currentWeather = this.processor.processCurrentWeatherRequest(latitude, longitude);
        } catch (CurrentWeatherException e) {
            LOGGER.error("Error retrieving current weather for latitude {}, longitude {} from OpenWeatherMap " +
                            "Current Weather API. Response status code: {}.",
                    latitude, longitude, e.getStatusCode(), e);
        }

        if (currentWeather != null) {
            LOGGER.info("Successfully retrieved current weather for latitude {}, longitude {}", latitude, longitude);
            return new ResponseEntity<CurrentWeather>(currentWeather, HttpStatus.OK);
        }

        return new ResponseEntity<String>(
                String.format("Error retrieving current weather for latitude %s, longitude %s", latitude, longitude),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * REST API for getting an hourly weather forecast for the next 48 hours, for the specified location
     *
     * @param latitude  the latitude of the location to get the hourly forecast for
     * @param longitude the longitude of the location to get the hourly forecast for
     * @return an {@link HourlyWeatherForecastResponse} object in JSON format
     */
    @GetMapping(path = "/weather-forecast/hourly", produces = "application/json")
    public ResponseEntity<?> getHourlyWeatherForecast(
            @RequestParam(name = "latitude", required = true) String latitude,
            @RequestParam(name = "longitude", required = true) String longitude) {
        LOGGER.info("Received request at /weather-forecast/hourly endpoint. Latitude: {}. Longitude: {}.",
                latitude, longitude);

        HourlyWeatherForecastResponse hourlyForecast = null;

        try {
            hourlyForecast = (HourlyWeatherForecastResponse) this.processor.processWeatherForecastRequest(
                    latitude, longitude, ForecastType.HOURLY);
        } catch (WeatherForecastException e) {
            LOGGER.error("Error retrieving hourly weather forecast for lat {}, long {} from OpenWeatherMap One Call API. "
                    + "Response status code: {}.", latitude, longitude, e.getStatusCode(), e);
            return new ResponseEntity<String>(
                    String.format("Error retrieving hourly weather forecast for latitude %s, longitude %s",
                            latitude, longitude),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }

        LOGGER.info("Successfully retrieved hourly weather forecast for lat {} long {}", latitude, longitude);
        return new ResponseEntity<HourlyWeatherForecastResponse>(hourlyForecast, HttpStatus.OK);
    }

    /**
     * REST API for getting a daily weather forecast for the next 7 days, for the specified location
     *
     * @param latitude  the latitude of the location to get the daily forecast for
     * @param longitude the longitude of the location to get the daily forecast for
     * @return a {@link DailyWeatherForecastResponse} object in JSON format
     */
    @GetMapping(path = "/weather-forecast/daily", produces = "application/json")
    public ResponseEntity<?> getDailyWeatherForecast(
            @RequestParam(name = "latitude", required = true) String latitude,
            @RequestParam(name = "longitude", required = true) String longitude) {
        LOGGER.info("Received request at /weather-forecast/daily endpoint. Latitude: {}. Longitude: {}.",
                latitude, longitude);

        DailyWeatherForecastResponse dailyForecast = null;

        try {
            dailyForecast = (DailyWeatherForecastResponse) this.processor.processWeatherForecastRequest(
                    latitude, longitude, ForecastType.DAILY);
        } catch (WeatherForecastException e) {
            LOGGER.error("Error retrieving daily weather forecast for lat {}, long {} from OpenWeatherMap One Call API. "
                    + "Response status code: {}.", latitude, longitude, e.getStatusCode(), e);
            return new ResponseEntity<String>(
                    String.format("Error retrieving daily weather forecast for latitude %s, longitude %s",
                            latitude, longitude),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }

        LOGGER.info("Successfully retrieved daily weather forecast for lat {} long {}", latitude, longitude);
        return new ResponseEntity<DailyWeatherForecastResponse>(dailyForecast, HttpStatus.OK);
    }

    /**
     * REST API to start a new weather update task for a particular user
     *
     * @param weatherUpdateRequest contains the user ID and time interval for the new weather update task
     * @return a {@link HttpStatus#OK} response
     */
    @PostMapping(path = "/weather-update", consumes = "application/json")
    public ResponseEntity<Void> startWeatherUpdateTask(@Valid @RequestBody WeatherUpdateRequest weatherUpdateRequest) {
        int userId = weatherUpdateRequest.getUserId();
        int intervalSeconds = weatherUpdateRequest.getIntervalSeconds();
        LOGGER.info("Received request at /weather-update endpoint to start a new weather update task. " +
                        "User ID: {}. Task interval (s): {}.",
                userId, intervalSeconds);

        this.weatherUpdateScheduler.scheduleTask(userId, intervalSeconds);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    /**
     * REST API to stop an existing weather update task that has the specified ID
     *
     * @param userId the ID of the user that the weather update task corresponds to
     * @return a {@link HttpStatus#OK} response
     */
    @DeleteMapping(path = "/weather-update/{userId}")
    public ResponseEntity<Void> stopWeatherUpdateTask(@PathVariable int userId) {
        LOGGER.info("Received request at /weather-update endpoint to stop an existing weather update task for user {}.",
                userId);

        this.weatherUpdateScheduler.stopTask(userId);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
